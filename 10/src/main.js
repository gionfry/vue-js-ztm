import { createApp } from "vue";
import App from "./App.vue";
// import Greeting from "@/components/Greeting.vue"

// il nome nell'import è messo uguale all' nome del componente per pratica comune per trovare più semplicemente il comenente
// ma lo si può nomimare come vuoi

let vm = createApp(App)

// non dobbiamo avere paura di passarlo come un ogetto perche webpack lo andra a compilare cosi,
// vue andra a capire cosa stiamo cercando di fare

// vm.component("Greeting", Greeting)

// qui è come riegistriamo un componente globale
// un componente globale può essere usato ovunque in vue questo include i componenti figli, ma non è raccomandato di farlo

vm.mount("#app");

// noi andiamo a evitare di fare componenti globali ma solo locali per non sovracaricare i module blundlers come webpack, 
// cosi di solito si va ad usare componenti locali come best practise