// creiamo una funzione per inizializzare una applicazione, tiene traccia dei dati ed modifica il documento
// dove deve andare a mettersi l'applicazione con mount
const vm = Vue.createApp({
    data() {
        return {
            fristName: 'Mattia',
            lastName: 'cisco'
        }
    },
    //  è una proprietà dove posso definire funzioni per la nostra applicazione ed è simile al data object
    methods: {

    },
}).mount("#app")

// setTimeout(() => { vm.fristName = 'maurizio'; }, 2000);
// chiamo il componente vue come costante ed viene usato vm per view Model


Vue.createApp({
    data() {
        return {
            fristName: 'Mario',
            lastName: 'Rossi'
        }
    },
}).mount("#app2")

Vue.createApp({
    data() {
        return {
            fristName: 'Mattia',
            middleName: '',
            lastName: 'cisco',
            url: 'https://google.com',
            raw_url: '<a href="https://google.com" target="_blank" class="">Google 2.0</a>',
            eta: 25,
        }
    },
    methods: {
        // non funzione con arrow function => ma solo con la normale funzione
        // fullName() {
        //     console.log('Full name è stata chiamata')
        //     return `${this.fristName} ${this.middleName} ${this.lastName.toUpperCase()}`
        // },
        increment() {
            this.eta++
        },

        updateMiddlename(event) {
            this.middleName = event.target.value
        },

        // vue ha un funzione di modifica evento
        updateLastName(msg, event) {
            // event.preventDefault();
            // console.log(msg)
            this.lastName = event.target.value
        }
    },
    // vue chache i valori di un coputed valore ed la funzione full name non viene attivata ogni volta anche dal cambio di età
    computed: {
        fullName() {
            console.log('Full name è stata chiamata da computed')
            return `${this.fristName} ${this.middleName} ${this.lastName.toUpperCase()}`
        },
    },
    // puoi vedere tutti i valori o danti anche dentro le proprietà di computed
    // proprietà di computed non potranno mai essere o non dovrebbero essere asyncrone ma watch si può!
    watch: {
        eta(newVal, oldVal) {
            setTimeout(() => { this.eta = 25 }, 3000)
        }
    },
}).mount(".app")