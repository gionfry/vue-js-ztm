// let vm = Vue.createApp({
//   data() {
//     return {
//       message: "Hello world!"
//     }
//   },
//   beforeCreate() {
//     console.log('beforeCreate() funzione chiamata!', this.message)
//   },
//   created() {
//     console.log('created() funzione chiamata!', this.message)

//   },
//   beforeMount() {
//     // $el elemento dove vue è stato montato, è disponibile dopo che vue è stato montato mount
//     console.log('beforeMount() funzione chiamata!', this.$el)

//   },
//   mounted() {
//     console.log('mounted() funzione chiamata!', this.$el)

//   },
//   beforeUpdate() {
//     console.log('beforeUpdate funzione chiamata!')

//   },
//   updated() {
//     console.log('updated() funzione chiamata!')

//   },
//   beforeUnmount() {
//     console.log('beforeUnmount() funzione chiamata!')

//   },
//   unmounted() {
//     console.log('unmounted() funzione chiamata!')

//   },

// })
// vm.mount('#app')
// setTimeout(() => { vm.mount('#app') }, 3000)




let vm = Vue.createApp({
  //  data() {
  //   return {
  //     message: "Hello world!"
  //   }
  // },

  // template: `{{ message }}`
})

// componenti devono essere impostati prima del mount application
// la funzione è disposnibile dopo aver creato applicazione di vue ma prima del mount
// si puo' scirvere in Pascal o kebab case

vm.component('ciao', {
  template: `<h1>{{message}}</h1>`,
  data() {
    return {
      message: 'Ciaoo Mondo'
    }
  },
})
// le istanze di data e i nostri componenti sono isolati da un altra vista non manderà un errore
// per avevere proprietà di data con nomi simili che si muovo insieme

vm.mount('#app')

// let vm2 = Vue.createApp({
//   data() {
//     return {
//       message: "Hello world!"
//     }
//   },
//   render() {
//     // h sta per hypertext
//     return Vue.h(
//       'h1',
//       this.message
//     )
//   },
// }).mount('#app2')





